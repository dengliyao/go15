package blog

import (
	"encoding/json"
	"time"
)

// 构造函数, 对对象进行初始化, 统一对对象的初始者进行管理,
// 保证构造的对象 是可用的, 不容易出现nil
func NewBlog() *Blog {
	return &Blog{
		&Meta{
			CreatedAt: time.Now().Unix(),
		},
		&CreateBlogRequest{
			Tags: map[string]string{},
		},
		&ChangedBlogStatusRequest{},
	}
}

type Blog struct {
	*Meta
	*CreateBlogRequest
	*ChangedBlogStatusRequest
}

func (req *Blog) String() string {
	dj, _ := json.MarshalIndent(req, "", "	")
	return string(dj)
}

// 通用参数
type Meta struct {
	// 用户Id
	Id int `json:"id" gorm:"column:id"`
	// 创建时间, 时间戳 10位, 秒
	CreatedAt int64 `json:"created_at" gorm:"column:created_at"`
	// 更新时间, 时间戳 10位, 秒
	UpdatedAt int64 `json:"updated_at" gorm:"column:updated_at"`
}

// `title` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '文章标题',
// `author` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '作者',
// `content` text CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '文章内容',
// `summary` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '文章概要信息',
// `create_by` varchar(255) COLLATE utf8mb4_general_ci NOT NULL COMMENT '创建人',
// `tags` text CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '标签',
type CreateBlogRequest struct {
	// 文章标题
	Title string `json:"title" gorm:"column:title" validate:"required"`
	// 作者
	Author string `json:"author" gorm:"column:author" validate:"required"`
	// 文章内容
	Content string `json:"content" gorm:"column:content" validate:"required"`
	// 文章概要信息
	Summary string `json:"summary" gorm:"column:summary"`
	// 创建人
	CreateBy string `json:"create_by" gorm:"column:create_by"`
	// 标签 https://gorm.io/docs/serializer.html
	Tags map[string]string `json:"tags" gorm:"column:tags;serializer:json"`
}

func (req *CreateBlogRequest) String() string {
	dj, _ := json.MarshalIndent(req, "", "	")
	return string(dj)
}

// 发布才能修改文章状态
// `published_at` int NOT NULL COMMENT '发布时间',
// `status` tinyint NOT NULL COMMENT '文章状态: 草稿/已发布',
type ChangedBlogStatusRequest struct {
	// 发布时间
	PublishedAt int64 `json:"published_at" gorm:"column:published_at"`
	// 文章状态: 草稿/已发布
	Status Status `json:"status" gorm:"column:status"`
}

func (req *ChangedBlogStatusRequest) String() string {
	dj, _ := json.MarshalIndent(req, "", "	")
	return string(dj)
}
